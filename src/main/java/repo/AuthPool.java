package repo;

import pojo.AuthUser;
import pojo.Role;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AuthPool {
    private ConcurrentHashMap<UUID, AuthUser> authUserMap = new ConcurrentHashMap<>();

    public void add(UUID uuid, AuthUser value) {
        if (uuid != null && value != null) {
            authUserMap.put(uuid, value);
        }
    }
    public void deleteByUUID(UUID uuid) {
        if (uuid != null) {
            authUserMap.remove(uuid);
        }
    }

    public boolean findByUUID(UUID uuid) {
        return authUserMap.containsKey(uuid);
    }

    public Role getRoleByUUID (UUID uuid) {
        return authUserMap.get(uuid).getRole();
    }
}
