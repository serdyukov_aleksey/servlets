package repo;

import pojo.RegUser;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static pojo.Role.ROOT;

public class RegPool {
    private ConcurrentHashMap<String, RegUser> regUserMap = new ConcurrentHashMap<>();

    public RegPool() {
        regUserMap.put("RootUser", new RegUser("RootUser", "qwerty", "qwerty",
                "email@email.com", ROOT));
    }

    public void add(String login, RegUser value) {
        if (login != null && value != null) {
            regUserMap.put(login, value);
        }
    }

    public Optional<RegUser> findUserByLogin(String login) {
        return Optional.ofNullable(regUserMap.get(login));
    }

    public Optional<ArrayList<RegUser>> getAllUsers() {
        Map<String, RegUser> map = regUserMap;
        return Optional.of(new ArrayList<>(map.values()));
    }
}