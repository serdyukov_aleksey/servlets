package configuration;

import repo.AuthPool;
import repo.RegPool;
import utils.Validation;

public class AppContext {
    private static volatile AppContext appContext = new AppContext();
    private final RegPool regPool = new RegPool();
    private final AuthPool authPool = new AuthPool();
    private final Validation validation = new Validation(regPool);

    public static AppContext getInstance() {
        return appContext;
    }

    public RegPool getRegPool() {
        return regPool;
    }

    public AuthPool getAuthPool() {
        return authPool;
    }

    public Validation getValidation() {
        return validation;
    }
}
