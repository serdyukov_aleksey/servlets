package pojo;

import java.util.Objects;

public class AuthUser {
    private String login;
    private String password;
    private Role role;

    public AuthUser(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthUser authUser = (AuthUser) o;
        return Objects.equals(login, authUser.login) &&
                Objects.equals(password, authUser.password);
    }

    @Override
    public int hashCode() {
        final int PRIME = 7;
        int result = 1;
        result = PRIME * result + ((login == null) ? 0 : login.hashCode());
        return PRIME * result + ((password == null) ? 0 : password.hashCode());
    }

    @Override
    public String toString() {
        return "login= " + login +
                ", password= " + password;
    }
}
