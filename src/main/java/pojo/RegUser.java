package pojo;

import java.util.Objects;

public class RegUser {
    private String login;
    private String password;
    private String confirmPassword;
    private String email;
    private Role role;

    public RegUser(String login, String password, String confirmPassword, String email, Role role) {
        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.email = email;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getPassAgain() {
        return confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegUser)) return false;
        RegUser regUser = (RegUser) o;
        return Objects.equals(getLogin(), regUser.getLogin()) &&
                Objects.equals(getPassword(), regUser.getPassword()) &&
                Objects.equals(getPassAgain(), regUser.getPassAgain()) &&
                Objects.equals(getEmail(), regUser.getEmail());
//                getRole() == regUser.getRole();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogin(), getPassword(), getPassAgain(), getEmail());
    }
}
