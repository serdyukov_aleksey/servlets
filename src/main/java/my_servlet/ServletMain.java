package my_servlet;

import configuration.AppContext;
import repo.AuthPool;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;


public class ServletMain extends HttpServlet {

    AppContext appContext = AppContext.getInstance();
    private static final String APPLICATION_JSON = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String COOKIE_NAME = "token";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!APPLICATION_JSON.equalsIgnoreCase(req.getHeader(CONTENT_TYPE))) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid content type");
        } else {
            String param = req.getParameter("method");
            Cookie[] cookie = req.getCookies();
            AuthPool authPool = appContext.getAuthPool();
            switch (param) {
                case "onload":
                    for (Cookie c : cookie) {
                        if (c.getName().equals(COOKIE_NAME)) {
                            if (authPool.findByUUID(UUID.fromString(c.getValue()))) {
                                resp.setStatus(HttpServletResponse.SC_OK);
                                return;
                            }
                        }
                    }
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    break;
                case "logout":
                    for (Cookie c : cookie) {
                        if (c.getName().equals(COOKIE_NAME)) {
                            authPool.deleteByUUID(UUID.fromString(c.getValue()));
                            resp.setStatus(HttpServletResponse.SC_OK);
                            return;
                        }
                    }
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    break;
            }
        }
    }
}
