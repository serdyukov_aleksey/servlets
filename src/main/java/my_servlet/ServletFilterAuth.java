package my_servlet;

import com.google.gson.Gson;
import configuration.AppContext;
import main.ApplicationStarter;
import pojo.AuthUser;
import pojo.RegUser;
import pojo.Role;
import repo.AuthPool;
import repo.RegPool;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServletFilterAuth implements Filter {

    private final Gson gson = new Gson();
    private final AppContext appContext = AppContext.getInstance();
    private final Logger logger = LoggerFactory.getLogger(ApplicationStarter.class);


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Filter auth was started");
    }

    @Override
    public void destroy() {
        logger.info("Filter auth was stopped");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setCharacterEncoding("UTF-8");
        String body = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        AuthUser authUser = gson.fromJson(body, AuthUser.class);
        if (checkUserReg(authUser)) {
            UUID uuid = UUID.randomUUID();
            AuthPool authPool = appContext.getAuthPool();
            authPool.add(uuid, authUser);
            Cookie cookie = new Cookie("token", uuid.toString());
            resp.addCookie(cookie);
            filterChain.doFilter(servletRequest, resp);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write(gson.toJson("Invalid data"));
        }
    }

    private boolean checkUserReg(AuthUser user) {
        RegPool regPool = appContext.getRegPool();
        if (regPool.findUserByLogin(user.getLogin()).isPresent()){
            RegUser regUser = regPool.findUserByLogin(user.getLogin()).get();
            user.setRole(regUser.getRole());
            System.out.println(user.getRole());
            return user.getPassword().equals(regUser.getPassword());
        }
        return false;
    }
}