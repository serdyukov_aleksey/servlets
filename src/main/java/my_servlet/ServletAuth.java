package my_servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletAuth extends HttpServlet {
    private static final String APPLICATION_JSON = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!APPLICATION_JSON.equalsIgnoreCase(req.getHeader(CONTENT_TYPE))) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid content type");
        } else {
            resp.setStatus(HttpServletResponse.SC_OK);
        }
    }
}
