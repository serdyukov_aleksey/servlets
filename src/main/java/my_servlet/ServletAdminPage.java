package my_servlet;

import com.google.gson.Gson;
import configuration.AppContext;
import pojo.AuthUser;
import pojo.RegUser;
import pojo.Role;
import repo.AuthPool;
import repo.RegPool;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class ServletAdminPage extends HttpServlet {
    AppContext appContext = AppContext.getInstance();
    private static final String APPLICATION_JSON = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String COOKIE_NAME = "token";
    private static final Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!APPLICATION_JSON.equalsIgnoreCase(req.getHeader(CONTENT_TYPE))) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid content type");
        } else {
            Cookie[] cookie = req.getCookies();
            AuthPool authPool = appContext.getAuthPool();
            for (Cookie c : cookie) {
                if (c.getName().equals(COOKIE_NAME)) {
                    if (authPool.findByUUID(UUID.fromString(c.getValue()))) {
                        Role role = authPool.getRoleByUUID(UUID.fromString(c.getValue()));
                        if (role == Role.ADMIN || role == Role.ROOT) {
                            resp.setStatus(HttpServletResponse.SC_OK);
                            resp.getWriter().write(gson.toJson(getAllUsers()));
                            return;
                        }
                    }
                }
            }
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    private ArrayList<RegUser> getAllUsers() {
        RegPool regPool = appContext.getRegPool();
        if (regPool.getAllUsers().isPresent()) {
            return regPool.getAllUsers().get();
        }
        return null;
    }
}
