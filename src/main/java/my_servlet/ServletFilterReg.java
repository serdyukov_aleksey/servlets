package my_servlet;

import com.google.gson.Gson;
import configuration.AppContext;
import main.ApplicationStarter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.RegUser;
import pojo.Role;
import repo.RegPool;
import utils.Validation;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;
import java.util.stream.Collectors;

public class ServletFilterReg implements Filter {
    private final AppContext appContext = AppContext.getInstance();
    private final Gson gson = new Gson();
    RegPool regPool = appContext.getRegPool();
    private final Logger logger = LoggerFactory.getLogger(ApplicationStarter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Filter reg was started");
    }

    @Override
    public void destroy() {
        logger.info("Filter reg was stopped");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setCharacterEncoding("UTF-8");
        String body = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        RegUser regUser = gson.fromJson(body, RegUser.class);
        if (new Random().nextBoolean()) {
            regUser.setRole(Role.ADMIN);
        }
        else {
            regUser.setRole(Role.USER);
        }
        if(checkUserReg(regUser)) {
            regPool.add(regUser.getLogin(), regUser);
            filterChain.doFilter(servletRequest, resp);
        }else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write(gson.toJson("Not valid data"));
        }
    }

    private boolean checkUserReg(RegUser user) {
        Validation validation = appContext.getValidation();
        return validation.validateLogin(user.getLogin()) && validation.validatePassword(user.getPassword())
                && validation.validateEmail(user.getEmail()) && validation.isUniqueLogin(user.getLogin()) &&
                validation.comparePass(user);
    }
}
