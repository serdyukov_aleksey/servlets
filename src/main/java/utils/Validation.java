package utils;

import pojo.RegUser;
import repo.RegPool;

import java.util.Objects;
import java.util.regex.Pattern;

public class Validation {

    private final RegPool regPool;

    public Validation(RegPool regPool) {
        this.regPool = regPool;
    }

    public boolean validateLogin(String login) {
        if (Objects.nonNull(login) && !login.isEmpty()) {
            return Pattern.matches("^[a-zA-Z](?!.*\\.\\.)[a-zA-Z0-9@#$%^&+=.\\\\d]{5,12}$", login);
        }
        return false;
    }

    public boolean validatePassword(String password) {
        if (Objects.nonNull(password) && !password.isEmpty()) {
            return Pattern.matches("^[a-zA-Z0-9@#$%^&+=./\\-_(){}\\[\\]][a-zA-Z0-9@#$%^&+=.\\\\d]{5,11}$", password);
        }
        return false;
    }

    public boolean validateEmail(String email) {
        if (Objects.nonNull(email) && !email.isEmpty()) {
            return Pattern.matches("^[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$", email);
        }
        return false;
    }

    public boolean isUniqueLogin(String login) {
        return !regPool.findUserByLogin(login).isPresent();
    }

    public boolean comparePass(RegUser regUser) {
        return regUser.getPassword().equals(regUser.getPassAgain());
    }
}