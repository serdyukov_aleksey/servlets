<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 19.08.2020
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="application/json; charset=UTF-8">
    <title>Authorization</title>
</head>
<body>
<div class="title">
    <h1>Authorization</h1>
</div>
<form>
    <label>Login: <input name="login" id="login" type="text" placeholder="Login"></label>
    <br>
    <label>Password: <input name="password" id="password" type="password" placeholder="Password" autocomplete="on"></label>
    <br>
    <input name="btnSignIn" type="submit" value="Sign in" onclick="listener()">
    <input name="btnSignUp" type="button" value="Sign up" onclick='location.href="reg.jsp"'>
</form>
<br>
<div class="helper">
    <p id="errorDescription" class="demo" style="color: red"></p>
</div>
<script>
    function listener(){
        document.addEventListener('submit', (e) => {
            e.preventDefault();
            e.stopPropagation();
        });
        const xhr = new XMLHttpRequest();
        const url = "http://localhost:8080/chat/auth";
        xhr.open("POST", url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                window.open('main_page.jsp', '_self');
            } if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 400) {
                const errorDescription = document.getElementById("errorDescription")
                errorDescription.innerText = JSON.parse(xhr.responseText);
            }
        };
        xhr.setRequestHeader('Content-type', 'application/json')
        const user = {
            login: document.getElementById("login").value,
            password: document.getElementById("password").value,
        }
        xhr.send(JSON.stringify(user));
    }
</script>
</body>
</html>
