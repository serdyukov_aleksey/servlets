<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 19.08.2020
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<div class="title">
    <h1>Registration</h1>
</div>
<form>
    <label>Login:
        <input name="login" id="login" type="text" placeholder="Login"></label>
<br>
    <label>Password:
    <input name="password" id="password" type="password" placeholder="Password"></label>
<br>
    <label>Confirm password :
    <input name="passwordAg" id="passwordAg" type="password" placeholder="Password again"></label>
<br>
    <label>E-mail :
        <input name="email" id="email" type="email" placeholder="E-mail"></label>
<br>
    <input name="btnSignUp" type="button" value="Sign up" onclick="listener()">
</form>
<br>
<div class="helper">
<p id="errorDescription" class="demo" style="color: red"></p>
</div>
<script>
    function listener(){
        document.addEventListener('submit', (e) => {
            e.preventDefault();
            e.stopPropagation();
        });
        const xhr = new XMLHttpRequest();
        const url = "http://localhost:8080/chat/reg";
        xhr.open("POST", url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                window.open('auth.jsp', '_self');
            } if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 400) {
                const errorDescription = document.getElementById("errorDescription")
                errorDescription.innerText = JSON.parse(xhr.responseText);
            }
        };
        xhr.setRequestHeader('Content-type', 'application/json')
        const user = {
            login: document.getElementById("login").value,
            password: document.getElementById("password").value,
            confirmPassword: document.getElementById("passwordAg").value,
            email: document.getElementById("email").value
        }
        xhr.send(JSON.stringify(user));
        console.log(user);
    }
</script>
</body>
</html>
