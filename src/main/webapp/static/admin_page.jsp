<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 20.08.2020
  Time: 00:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Page</title>
</head>
<body>
<div>
    <p id="userList" class="user-list"></p>
</div>
<script>
    window.onload = function () {
        const textArea = document.getElementById('userList');
        const xhr = new XMLHttpRequest();
        const url = "http://localhost:8080/chat/admin";
        xhr.open("POST", url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 403) {
                window.open('main_page.jsp', '_self');
            } if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                textArea.innerText = xhr.response;
            }
        };
        xhr.setRequestHeader('Content-type', 'application/json')
        xhr.send(document.cookie);
    }
</script>
</body>
</html>
